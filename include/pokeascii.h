#pragma once

/**
 * @author Jorge Muñoz Taylor
 * @date 26/01/2020
 * 
 * @file pokeascii.h
 * @brief Dibuja en ASCII a los pokémons.
 */

/**
 * @brief Dibuja a Articuno, Moltres y Zapdos en ASCII.
 * @param numero_de_pokemon El número real en la lista de pokémons.
 */
void pokeascii ( int numero_de_pokemon );