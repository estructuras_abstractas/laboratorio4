#pragma once

/**
 * @author Jorge Muñoz Taylor
 * @date 26/01/2020
 * 
 * @file Articuno.h
 * @brief
 */

#include <string>
#include "Flying.h"
#include "Water.h"

/** @class Articuno
 *  @brief Clase concreta que define a un articuno.
 */

class Articuno : public Flying, public Water
{
    public:

        /**
         * @brief Constructor por defecto, inicializa todas las variables de la clase.
         */
        Articuno();


        /**
         * @brief Primer ataque.
         * @param other El pokémon al que atacará
         */
        void atk1 ( Pokemon &other );


        /**
         * @brief Segundo ataque.
         * @param other El pokémon al que atacará
         */
        void atk2 ( Pokemon &other );


        /**
         * @brief Tercer ataque.
         * @param other El pokémon al que atacará
         */
        void atk3 ( Pokemon &other );


        /**
         * @brief Cuarto ataque.
         * @param other El pokémon al que atacará
         */
        void atk4 ( Pokemon &other );


        /**
         * @brief Imprime las estadísticas de Articuno.
         */
        void print();


        /**
         * @brief Obtiene el tipo de pokémon de la clase flying.
         * @return El tipo del pokémon.
         */ 
        string type_flying();


        /**
         * @brief Obtiene las fortalezas del tipo flying.
         * @return Fortalezas del tipo flying.
         */ 
        string strongVs_flying();


        /**
         * @brief Obtiene las debilidades de la clase flying.
         * @return Debilidad de la clase flying.
         */ 
        string weakVs_flying();


        /**
         * @brief Obtiene el tipo de pokémon de la clase Water.
         * @return El tipo de la clase water.
         */ 
        string type_water();


        /**
         * @brief Obtiene las fortalezas de la clase water.
         * @return Fortaleza de la clase water.
         */ 
        string strongVs_water();


        /**
         * @brief Obtiene las debilidades de la clase water.
         * @return Debilidades de la clase water.
         */ 
        string weakVs_water();

    private:

        string Type; //< Tipo de pokémon.
        string strongVs; //< Fortalezas del pokémon.
        string weakVs; //< Debilidades del pokémon.
};