#pragma once

/**
 * @author Jorge Muñoz Taylor
 * @date 26/01/2020
 * 
 * @file Electric.h
 * @brief Define el archivo cabecera la clase Electric.
 */

#include <string>
#include "Pokemon.h"

using namespace std;

/** @class Electric
 *  @brief Clase abstracta que define las características básicas de un pokémon tipo eléctrico.
 */
class Electric : virtual public Pokemon
{
    public:

        /**
         * @brief Devuelve el tipo.
         */
        static string type ();
 

        /**
         * @brief Devuelve contra que tipos es fuerte.
         */ 
        static string strongVs ();


        /**
         * @brief Devuelve contra que tipos es débil.
         */
        static string weakVs ();
};