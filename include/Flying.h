#pragma once

/**
 * @author Jorge Muñoz Taylor
 * @date 26/01/2020
 * 
 * @file Flying.h
 * @brief Archivo cabecera de la clase Flying.
 */

#include <string>
#include "Pokemon.h"

using namespace std;

/** @class Flying
 *  @brief Clase abstracta que define las características básicas de un pokémon tipo volador.
 */
class Flying : virtual public Pokemon
{
    public:

        /**
         * @brief Regresa el tipo de la clase Flying
         */
        static string type ();


        /**
         * @brief Regresa a qué es fuerte la clase Flying.
         */        
        static string strongVs();
        
        
        /**
         * @brief Regresa a qué es débil la clase Flying.
         */        
        static string weakVs ();
};