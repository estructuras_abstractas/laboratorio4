#pragma once

/**
 * @author Jorge Muñoz Taylor
 * @date 26/01/2020
 * 
 * @file Zapdos.h
 * @brief Archivo cabecera de la clase Zapdos.
 */

#include "Electric.h"
#include "Flying.h"

/** @class Zapdos
 *  @brief Clase concreta que define a un Zapdos.
 */

class Zapdos : public Electric, public Flying
{
    public:

        /**
         * @brief Constructor por defecto, inicializa todas las variables de la clase.
         */
        Zapdos();


        /**
         * @brief Primer ataque.
         * @param other El pokémon al que atacará
         */
        void atk1 ( Pokemon &other );


        /**
         * @brief Segundo ataque.
         * @param other El pokémon al que atacará.
         */
        void atk2 ( Pokemon &other );
        
        
        /**
         * @brief Tercer ataque.
         * @param other El pokémon al que atacará.
         */        
        void atk3 ( Pokemon &other );
        
        
        /**
         * @brief Cuarto ataque.
         * @param other El pokémon al que atacará.
         */         
        void atk4 ( Pokemon &other );


        /**
         * @brief Imprime las estadísticas de Zapdos.
         */
        void print();
        
        
        /**
         * @brief Obtiene el tipo de pokémon de la clase flying.
         * @return El tipo del pokémon.
         */        
        string type_flying();
        
        
        /**
         * @brief Obtiene a que es fuerte la clasae Flying.
         * @return La fortaleza de la clase flying.
         */        
        string strongVs_flying();
        

        /**
         * @brief Obtiene a que es débil la clase flying.
         * @return Las debilidades de la clase flying.
         */        
        string weakVs_flying();


        /**
         * @brief Obtiene el tipo eléctrico de la clase electric.
         * @return El tipo de la clase electric.
         */
        string type_electric();
 

        /**
         * @brief Obtiene las fortalezas de la clase electric.
         * @return La fortaleza de la clase electric.
         */ 
        string strongVs_electric();



        /**
         * @brief Obtiene las debilidades de la clase electric.
         * @return Las debilidades de la clase electric.
         */        
        string weakVs_electric();

    private:

        string Type; //< El tipo de pokémon.
        string strongVs; //< A cuales tipos es fuerte el pokémon.
        string weakVs; //< A cuales tipos es débil el pokémon.
};