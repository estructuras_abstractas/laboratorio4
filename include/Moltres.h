#pragma once

/**
 * @author Jorge Muñoz Taylor
 * @date 26/01/2020
 * 
 * @file Moltres.h
 * @brief
 */

#include <string>
#include "Flying.h"
#include "Fire.h"

/** @class Moltres
 *  @brief Clase concreta que define a Moltres.
 */

class Moltres : public Fire, public Flying
{
    public:

        /**
         * @brief Coonstructor por defecto, inicializa todas las variables de la clase.
         */
        Moltres();


        /**
         * @brief Primer ataque.
         * @param other El pokémon al que atacará
         */
        void atk1 ( Pokemon &other );


        /**
         * @brief Segundo ataque.
         * @param other El pokémon al que atacará
         */
        void atk2 ( Pokemon &other );


        /**
         * @brief Tercer ataque.
         * @param other El pokémon al que atacará
         */
        void atk3 ( Pokemon &other );


        /**
         * @brief Cuarto ataque.
         * @param other El pokémon al que atacará
         */
        void atk4 ( Pokemon &other );


        /**
         * @brief Imprime las estadísticas de Moltres.
         */
        void print();


        /**
         * @brief Obtiene el tipo de pokémon de la clase flying.
         * @return El tipo del pokémon.
         */ 
        string type_flying();


        /**
         * @brief Obtiene las fortalezas pokémon de la clase flying.
         * @return La fortaleza pokémon.
         */ 
        string strongVs_flying();


        /**
         * @brief Obtiene las debilidades del pokémon de la clase flying.
         * @return La debilidad del pokémon.
         */ 
        string weakVs_flying();


        /**
         * @brief Obtiene el tipo de pokémon de la clase Fire.
         * @return El tipo del pokémon.
         */ 
        string type_fire();


        /**
         * @brief Obtiene las fortalezad del pokémon de la clase Fire.
         * @return Fortaleza del tipo fire.
         */ 
        string strongVs_fire();


        /**
         * @brief Obtiene las debilidades del pokémon de la clase Fire.
         * @return Debilidades del tipo fire.
         */ 
        string weakVs_fire();

    private:

        string Type; //< Tipo de pokémon.
        string strongVs; //< Fortalezas del pokémon.
        string weakVs; //< Debilidades del pokémon.
};