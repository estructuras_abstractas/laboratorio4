#pragma once

/**
 * @author Jorge Muñoz Taylor
 * @date 26/01/2020
 * 
 * @file Water.h
 * @brief Archivo cabecera de la clase Water.
 */

#include <string>
#include "Pokemon.h"

using namespace std;

/** @class Water
 *  @brief Clase abstracta que define las características básicas de un pokémon tipo agua.
 */
class Water : virtual public Pokemon
{
    public:

        static string type    ();
        static string strongVs();
        static string weakVs  ();
};