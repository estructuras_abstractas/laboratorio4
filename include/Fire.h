#pragma once

/**
 * @author Jorge Muñoz Taylor
 * @date 26/01/2020
 * 
 * @file Fire.h
 * @brief Define el archivo cabecera de la clase Fire.
 */

#include <string>
#include "Pokemon.h"

using namespace std;

/** @class Fire
 *  @brief Clase abstracta que define las características básicas de un pokémon tipo fuego.
 */
class Fire : virtual public Pokemon
{
    public:

        /**
         * @brief Devuelve el tipo de la clase Fire.
         */
        static string type ();
        
        
        /**
         * @brief Devuelve a qué es fuerte la clase Fire.
         */        
        static string strongVs ();
        

        /**
         * @brief Devuelve a qué es débil la clase Fire.
         */        
        static string weakVs ();
};