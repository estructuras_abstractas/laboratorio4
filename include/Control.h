/**
 * @author Jorge Muñoz Taylor
 * @date 26/01/2020
 * 
 * @file main.cpp
 * @brief La función principal del programa.
 */

#pragma once


class Control
{
    public:

        /**
         * @brief Constructor por defecto.
         */
        Control();


        /**
         * @brief Regresa un número aleatorio entre 1 y 4.
         * @return Un número aleatorio entre 1 y 4.
         */
        int num_aleatorio();


        /**
         * @brief Pausa el juego hasta que se presione enter.
         */
        void pausar_juego();


};