#pragma once

/**
 * @author Jorge Muñoz Taylor
 * @date 26/01/2020
 * 
 * @file Pokemon.h
 * @brief Define la clase abstracta Pokémon.
 */

#include <iostream>
#include <string>

using namespace std;

/** @class Pokemon
 *  @brief Clase abstracta que define las características básicas de un pokémon.
 */
class Pokemon
{
    public:

        /**
         * @brief Función virtual para el primer ataque del pokémon.
         * @param other Pokémon al que atacará.
         */
        virtual void atk1 ( Pokemon &other ) = 0;


        /**
         * @brief Función virtual para el segundo ataque del pokémon.
         * @param other Pokémon al que atacará.
         */
        virtual void atk2 ( Pokemon &other ) = 0;
        
        
        /**
         * @brief Función virtual para el tercer ataque del pokémon.
         * @param other Pokémon al que atacará.
         */        
        virtual void atk3 ( Pokemon &other ) = 0;
        
        
        /**
         * @brief Función virtual para el cuarto ataque del pokémon.
         * @param other Pokémon al que atacará.
         */        
        virtual void atk4 ( Pokemon &other ) = 0;
        
        
        /**
         * @brief Llama al pokémon al campo de batalla.
         * @return Una cadena indicando que se llamó al pokémon.
         */        
        string Call ();
        
        
        /**
         * @brief Imprime la información del pokémon en la terminal.
         */        
        void printInfo();


        /**
         * @brief Obtiene el nombre dle pokémon.
         * @return El nombre del pokémon.
         */
        string getName();
        
        
        /**
         * @brief Obtiene el HP del pokémon.
         * @return El HP del pokémon.
         */        
        int getHP();


        /**
         * @brief Modifica el valor del HP del pokémon.
         * @param other Pokémon al que atacará.
         */
        void setHP( int sumar_quitar );


        /**
         * @brief Obtiene la defensa del pokémon.
         * @return La defensa del pokémon.
         */
        int getDEF();


        /**
         * @brief Obtiene el nombre del primer ataque.
         * @return El nombre del primer ataque.
         */
        string getAtk1_Name ();


        /**
         * @brief Obtiene el nombre del segundo ataque.
         * @return El nombre del segundo ataque.
         */        
        string getAtk2_Name ();
        
        
        /**
         * @brief Obtiene el nombre del tercer ataque.
         * @return El nombre del tercer ataque.
         */        
        string getAtk3_Name ();
        
        
        /**
         * @brief Obtiene el nombre del cuarto ataque.
         * @return El nombre del cuarto ataque.
         */
        string getAtk4_Name ();

    protected:

        string name;
        string species;
        int HP;
        int ATK;
        int DEF;
        int sATK;
        int sDEF;
        int SPD;
        int EXP;
        string call; //< Llama al pokémon al campo de batalla.

        string atk1_name; //< Nombre del primer ataque.
        string atk2_name; //< Nombre del segundo ataque.
        string atk3_name; //< Nombre del tercer ataque.
        string atk4_name; //< Nombre del cuarto ataque.
};