ALL: packages comp doxygen

packages:
	@echo "**************************************************************************"
	@echo "** Se instalarán los paquetes necesarios para que funciones el programa **"
	@echo "**************************************************************************"
	@sleep 1s

	sudo apt-get install build-essential -y
	sudo apt install doxygen -y
	sudo apt install doxygen-gui -y
	sudo apt-get install graphviz -y
	sudo apt install texlive-latex-extra -y
	sudo apt install texlive-lang-spanish -y
	sudo apt-get install libsfml-dev -y
	sudo apt autoremove -y


comp:
	@echo "**************************************************************************"
	@echo "***********           Se generará el ejecutable            ***************"
	@echo "**************************************************************************"
	@sleep 1s

	mkdir -p bin build docs

	g++ -D_GLIBCXX_USE_CXX11_ABI=0 -o ./build/Pokemon.o -c ./src/Pokemon.cpp
	g++ -D_GLIBCXX_USE_CXX11_ABI=0 -o ./build/pokeascii.o -c ./src/pokeascii.cpp

	g++ -D_GLIBCXX_USE_CXX11_ABI=0 -o ./build/Water.o -c ./src/Water.cpp
	g++ -D_GLIBCXX_USE_CXX11_ABI=0 -o ./build/Flying.o -c ./src/Flying.cpp
	g++ -D_GLIBCXX_USE_CXX11_ABI=0 -o ./build/Articuno.o -c ./src/Articuno.cpp

	g++ -D_GLIBCXX_USE_CXX11_ABI=0 -o ./build/Fire.o -c ./src/Fire.cpp
	g++ -D_GLIBCXX_USE_CXX11_ABI=0 -o ./build/Moltres.o -c ./src/Moltres.cpp

	g++ -D_GLIBCXX_USE_CXX11_ABI=0 -o ./build/Electric.o -c ./src/Electric.cpp
	g++ -D_GLIBCXX_USE_CXX11_ABI=0 -o ./build/Zapdos.o -c ./src/Zapdos.cpp

	g++ -D_GLIBCXX_USE_CXX11_ABI=0 -o ./build/Control.o -c ./src/Control.cpp

	g++ -D_GLIBCXX_USE_CXX11_ABI=0 -o ./build/main.o -c ./src/main.cpp
	
	g++ -D_GLIBCXX_USE_CXX11_ABI=0 -o ./bin/lab4 ./build/main.o ./build/Control.o ./build/Pokemon.o ./build/pokeascii.o ./build/Flying.o ./build/Water.o ./build/Fire.o ./build/Articuno.o ./build/Moltres.o ./build/Electric.o ./build/Zapdos.o

doxygen:
	@echo "**************************************************************************"
	@echo "************      Generando el archivo refman.pdf          ***************"
	@echo "**************************************************************************"
	@sleep 1s

	mkdir -p docs
	doxygen Doxyfile
	make -C ./docs/latex