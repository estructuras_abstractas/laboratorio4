/**
 * @author Jorge Muñoz Taylor
 * @date 26/01/2020
 * 
 * @file Water.cpp
 * @brief
 */

#include "../include/Water.h"


string Water::type()
{
    return "water";
}

string Water::strongVs()
{
   return "fire";
}

string Water::weakVs()
{
    return "water";
}