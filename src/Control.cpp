/**
 * @author Jorge Muñoz Taylor
 * @date 26/01/2020
 * 
 * @file Control.cpp
 * @brief La función principal del programa.
 */

#include <iostream>
#include <random>
#include <stdio_ext.h>
#include "../include/Articuno.h"
#include "../include/Moltres.h"
#include "../include/Zapdos.h"
#include "../include/Control.h"

using namespace std;

#define POKEDEX 1
#define POKEBATALLA 2
#define SALIR 3
#define JUGADOR 0 //< Si juega el jugador.
#define CPU 1 //< Si juega el CPU.
#define HP_MINIMA 0
#define NUMERO_DE_HABILIDADES 4
#define NUMERO_DE_JUGADORES 2

#define ATAQUE_1 1
#define ATAQUE_2 2
#define ATAQUE_3 3
#define ATAQUE_4 4



int Control::num_aleatorio(){

    /**
     * Genera y devuelve un número pseudoaleatorio.
     */
    random_device generator; 
    mt19937 gen(generator());
    uniform_int_distribution<> distribution(1,4);

    int dado = distribution(generator);
    
    return (dado);
}



void Control::pausar_juego()
{    
    __fpurge(stdin); ///< Limpia el buffer de cin.
    
    cout << endl;
    cout << "** Presione ENTER para continuar con el siguiente turno **";
    cin.ignore(); ///< Espera hasta que el usuario oprima enter.
       
    cout << endl;
}



Control::Control()
{
    bool continuar = true;
    bool continuar_escogencia;
    bool continuar_batalla;
    bool continua_jugador;
    int opcion;
    int escogido;
    int primer_escogido;
    int round = 1;
    int juega_jugador;
    int ataque;

    Articuno articuno;
    Moltres moltres;
    Zapdos zapdos;

    Pokemon* pokemon [ NUMERO_DE_JUGADORES ];


    cout << endl << "** Escoja 2 pokémones: **" << endl << endl;

    for ( int i=0; i<2; i++)
    {
        continuar_escogencia = true;
            
        while( continuar_escogencia == true )
        {
            cin.clear();
            __fpurge(stdin);

            cout << endl;
            cout << "--> 1. Articuno" << endl;
            cout << "--> 2. Moltres" << endl;
            cout << "--> 3. Zapdos" << endl;
            cin >> escogido;
            cout << endl;

            if( !cin )
            {
                cout << "Escriba un número pofa :/" << endl;
            }
            else
            {    
                if( primer_escogido != escogido )
                {            
                    if ( escogido == 1 ) 
                    {
                        pokemon [i] = &articuno;
                        continuar_escogencia = false;
                    } 
                    else if ( escogido == 2 ) 
                    {
                        pokemon [i] = &moltres;
                        continuar_escogencia = false;
                    }
                    else if ( escogido == 3 )
                    {
                        pokemon [i] = &zapdos;
                        continuar_escogencia = false;
                    }
                    else
                    {
                        cout << "Esa opción no existe :/" << endl;
                    }

                }
                else cout << "Escoja un pokémon diferente :v" << endl;
            }//Fin de else

            primer_escogido = escogido;

        }//Fin de while

    }//Fin de for

    cout << endl;
    cout << pokemon[JUGADOR]->Call();
    cout << endl << endl;
    cout << pokemon[CPU]->Call();
    cout << endl;

    while( continuar == true )
    {
        cin.clear();
        __fpurge(stdin);

        cout << endl << endl;
        cout << "** Escoja una acción :) **" << endl << endl;
        cout << " 1. Pokedex" << endl;
        cout << " 2. ¡Iniciar pokebatalla!" << endl;
        cout << " 3. Salir" << endl;
        cin >> opcion;

        if (!cin) cout << " Escriba un número entre 1 y 3 please :/" << endl;

        else
        {
            switch ( opcion )
            {
            case POKEDEX:
                
                cout << endl;
                cout << "=================================================" << endl << endl;
                
                for ( int i=0; i<NUMERO_DE_JUGADORES; i++)
                {
                    if (pokemon[i]->getName() == "Articuno" ) articuno.print();
                    if (pokemon[i]->getName() == "Moltres" )  moltres.print();
                    if (pokemon[i]->getName() == "Zapdos" )   zapdos.print();
                }
            
                cout << "=================================================" << endl;
                break;


            case POKEBATALLA:
                
                continuar_batalla = true;
                juega_jugador = JUGADOR;

                while ( continuar_batalla == true )
                {
                    cout << endl << endl << "****************************" << endl;
                    cout << "Turno " << round << endl;
                    cout << "****************************" << endl;

                    cout << "** " << pokemon[JUGADOR]->getName() << ": " << pokemon[JUGADOR]->getHP() << " HP" << endl;
                    cout << "** " << pokemon[CPU]->getName() << ": " << pokemon[CPU]->getHP() << "HP" << endl;
                    
                    /**
                     * Juega el jugador
                     */
                    if( juega_jugador == JUGADOR )
                    {
                        continua_jugador = true;

                        while( continua_jugador == true)
                        {
                            cin.clear();
                            __fpurge(stdin);

                            cout << endl << "Escoja un pokeataque para "<< pokemon[JUGADOR]->getName() << "!!!!" << endl;
                            cout << endl;
                            cout << " 1. " << pokemon[JUGADOR]->getAtk1_Name() << endl;
                            cout << " 2. " << pokemon[JUGADOR]->getAtk2_Name() << endl;
                            cout << " 3. " << pokemon[JUGADOR]->getAtk3_Name() << endl;
                            cout << " 4. " << pokemon[JUGADOR]->getAtk4_Name() << endl;
                            cin >> ataque;

                            cout << endl;

                            if( !cin )
                            { 
                                cout << " Escriba un número :/" << endl;
                            }                 

                            else
                            {
                                if( ataque >= 1 && ataque <= NUMERO_DE_HABILIDADES )
                                {
                                    if (pokemon[juega_jugador]->getName() == "Articuno" ) 
                                    { 
                                        if( pokemon[CPU]->getName() == "Articuno" )
                                        {
                                            if( ataque == ATAQUE_1 ) articuno.atk1( articuno );
                                            if( ataque == ATAQUE_2 ) articuno.atk2( articuno );
                                            if( ataque == ATAQUE_3 ) articuno.atk3( articuno );
                                            if( ataque == ATAQUE_4 ) articuno.atk4( articuno );
                                        }
                                        else if( pokemon[CPU]->getName() == "Moltres" )
                                        {
                                            if( ataque == ATAQUE_1 ) articuno.atk1( moltres );
                                            if( ataque == ATAQUE_2 ) articuno.atk2( moltres );
                                            if( ataque == ATAQUE_3 ) articuno.atk3( moltres );
                                            if( ataque == ATAQUE_4 ) articuno.atk4( moltres );
                                        }
                                        else if( pokemon[CPU]->getName() == "Zapdos" )
                                        {
                                            if( ataque == ATAQUE_1 ) articuno.atk1( zapdos );
                                            if( ataque == ATAQUE_2 ) articuno.atk2( zapdos );
                                            if( ataque == ATAQUE_3 ) articuno.atk3( zapdos );
                                            if( ataque == ATAQUE_4 ) articuno.atk4( zapdos );
                                        }
                                    }

                                    else if (pokemon[juega_jugador]->getName() == "Moltres" )  
                                    {
                                        if( pokemon[CPU]->getName() == "Articuno" )
                                        {
                                            if( ataque == ATAQUE_1 ) moltres.atk1( articuno );
                                            if( ataque == ATAQUE_2 ) moltres.atk2( articuno );
                                            if( ataque == ATAQUE_3 ) moltres.atk3( articuno );
                                            if( ataque == ATAQUE_4 ) moltres.atk4( articuno );
                                        }
                                        else if( pokemon[CPU]->getName() == "Moltres" )
                                        {
                                            if( ataque == ATAQUE_1 ) moltres.atk1( moltres );
                                            if( ataque == ATAQUE_2 ) moltres.atk2( moltres );
                                            if( ataque == ATAQUE_3 ) moltres.atk3( moltres );
                                            if( ataque == ATAQUE_4 ) moltres.atk4( moltres );
                                        }
                                        else if( pokemon[CPU]->getName() == "Zapdos" )
                                        {
                                            if( ataque == ATAQUE_1 ) moltres.atk1( zapdos );
                                            if( ataque == ATAQUE_2 ) moltres.atk2( zapdos );
                                            if( ataque == ATAQUE_3 ) moltres.atk3( zapdos );
                                            if( ataque == ATAQUE_4 ) moltres.atk4( zapdos );
                                        }
                                    }

                                    else if (pokemon[juega_jugador]->getName() == "Zapdos" )   
                                    {
                                        if( pokemon[CPU]->getName() == "Articuno" )
                                        {
                                            if( ataque == ATAQUE_1 ) zapdos.atk1( articuno );
                                            if( ataque == ATAQUE_2 ) zapdos.atk2( articuno );
                                            if( ataque == ATAQUE_3 ) zapdos.atk3( articuno );
                                            if( ataque == ATAQUE_4 ) zapdos.atk4( articuno );
                                        }
                                        else if( pokemon[CPU]->getName() == "Moltres" )
                                        {
                                            if( ataque == ATAQUE_1 ) zapdos.atk1( moltres );
                                            if( ataque == ATAQUE_2 ) zapdos.atk2( moltres );
                                            if( ataque == ATAQUE_3 ) zapdos.atk3( moltres );
                                            if( ataque == ATAQUE_4 ) zapdos.atk4( moltres );
                                        }
                                        else if( pokemon[CPU]->getName() == "Zapdos" )
                                        {
                                            if( ataque == ATAQUE_1 ) zapdos.atk1( zapdos );
                                            if( ataque == ATAQUE_2 ) zapdos.atk2( zapdos );
                                            if( ataque == ATAQUE_3 ) zapdos.atk3( zapdos );
                                            if( ataque == ATAQUE_4 ) zapdos.atk4( zapdos );
                                        }                    
                                    }

                                    continua_jugador = false;
                                }
                                else cout << "Ese ataque no existe :/" << endl;

                            }//Fin de else

                        }//Fin de while

                        cout << endl;
                        pausar_juego();
                    }


                    /**
                     * Juega el CPU
                     */
                    else if ( juega_jugador == CPU )
                    {
                        ataque = num_aleatorio();

                        cout << endl;
                    
                        if (pokemon[juega_jugador]->getName() == "Articuno" ) 
                        { 
                            if( pokemon[JUGADOR]->getName() == "Articuno" )
                            {
                                if( ataque == ATAQUE_1 ) articuno.atk1( articuno );
                                if( ataque == ATAQUE_2 ) articuno.atk2( articuno );
                                if( ataque == ATAQUE_3 ) articuno.atk3( articuno );
                                if( ataque == ATAQUE_4 ) articuno.atk4( articuno );
                            }
                            else if( pokemon[JUGADOR]->getName() == "Moltres" )
                            {
                                if( ataque == ATAQUE_1 ) articuno.atk1( moltres );
                                if( ataque == ATAQUE_2 ) articuno.atk2( moltres );
                                if( ataque == ATAQUE_3 ) articuno.atk3( moltres );
                                if( ataque == ATAQUE_4 ) articuno.atk4( moltres );
                            }
                            else if( pokemon[JUGADOR]->getName() == "Zapdos" )
                            {
                                if( ataque == ATAQUE_1 ) articuno.atk1( zapdos );
                                if( ataque == ATAQUE_2 ) articuno.atk2( zapdos );
                                if( ataque == ATAQUE_3 ) articuno.atk3( zapdos );
                                if( ataque == ATAQUE_4 ) articuno.atk4( zapdos );
                            }
                        }

                        else if (pokemon[juega_jugador]->getName() == "Moltres" )  
                        {
                            if( pokemon[JUGADOR]->getName() == "Articuno" )
                            {
                                if( ataque == ATAQUE_1 ) moltres.atk1( articuno );
                                if( ataque == ATAQUE_2 ) moltres.atk2( articuno );
                                if( ataque == ATAQUE_3 ) moltres.atk3( articuno );
                                if( ataque == ATAQUE_4 ) moltres.atk4( articuno );
                            }
                            else if( pokemon[JUGADOR]->getName() == "Moltres" )
                            {
                                if( ataque == ATAQUE_1 ) moltres.atk1( moltres );
                                if( ataque == ATAQUE_2 ) moltres.atk2( moltres );
                                if( ataque == ATAQUE_3 ) moltres.atk3( moltres );
                                if( ataque == ATAQUE_4 ) moltres.atk4( moltres );
                            }
                            else if( pokemon[0]->getName() == "Zapdos" )
                            {
                                if( ataque == ATAQUE_1 ) moltres.atk1( zapdos );
                                if( ataque == ATAQUE_2 ) moltres.atk2( zapdos );
                                if( ataque == ATAQUE_3 ) moltres.atk3( zapdos );
                                if( ataque == ATAQUE_4 ) moltres.atk4( zapdos );
                            }
                        }

                        else if (pokemon[juega_jugador]->getName() == "Zapdos" )   
                        {
                            if( pokemon[JUGADOR]->getName() == "Articuno" )
                            {
                                if( ataque == ATAQUE_1 ) zapdos.atk1( articuno );
                                if( ataque == ATAQUE_2 ) zapdos.atk2( articuno );
                                if( ataque == ATAQUE_3 ) zapdos.atk3( articuno );
                                if( ataque == ATAQUE_4 ) zapdos.atk4( articuno );
                            }
                            else if( pokemon[JUGADOR]->getName() == "Moltres" )
                            {
                                if( ataque == ATAQUE_1 ) zapdos.atk1( moltres );
                                if( ataque == ATAQUE_2 ) zapdos.atk2( moltres );
                                if( ataque == ATAQUE_3 ) zapdos.atk3( moltres );
                                if( ataque == ATAQUE_4 ) zapdos.atk4( moltres );
                            }
                            else if( pokemon[JUGADOR]->getName() == "Zapdos" )
                            {
                                if( ataque == ATAQUE_1 ) zapdos.atk1( zapdos );
                                if( ataque == ATAQUE_2 ) zapdos.atk2( zapdos );
                                if( ataque == ATAQUE_3 ) zapdos.atk3( zapdos );
                                if( ataque == ATAQUE_4 ) zapdos.atk4( zapdos );
                            }                    
                        }

                        cout << endl;
                        pausar_juego();
                    }
                    


                    if( pokemon[JUGADOR]->getHP() <= HP_MINIMA ) 
                    {
                        cout << endl << endl;

                        cout << pokemon[CPU]->getName() << " venció !!!!!! :D";

                        cout << endl << endl;
                        continuar = false;
                        continuar_batalla = false;
                    }

                    if( pokemon[CPU]->getHP() <= HP_MINIMA ) 
                    {
                        cout << endl << endl;
                    
                        cout << pokemon[JUGADOR]->getName() << " venció !!!!!! :D";

                        cout << endl << endl;
                        continuar = false;
                        continuar_batalla = false;
                    }

                    if (juega_jugador == JUGADOR) juega_jugador = CPU;
                    else juega_jugador = JUGADOR;

                    round++;
                }//Fin de while

                break;


            case SALIR:
                cout << "Chau :>";
                continuar = false;
                break;


            default:

                cout << "Esa opción no existe :3" << endl;
                continuar = true;
                break;
            }
        }

    }//Fin de while

}