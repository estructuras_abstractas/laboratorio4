/**
 * @author Jorge Muñoz Taylor
 * @date 26/01/2020
 * 
 * @file Pokemon.h
 * @brief Definición de la clase abstracta Pokémon.
 */

#include "../include/Pokemon.h"


string Pokemon::Call ()
{
    string invocar;
    invocar =  "---> ¡" + this->name + " a sido invocado!";
    
    return invocar;
}


void Pokemon::printInfo()
{
    cout << " Pokedex: " << endl;
    cout << endl << "--> Nombre:           " << this->name;
    cout << endl << "--> Especie:          " << this->species;
    cout << endl << "--> HP:               " << this->HP;
    cout << endl << "--> Ataque:           " << this->ATK;
    cout << endl << "--> Defensa:          " << this->DEF;
    cout << endl << "--> Ataque especial:  " << this->sATK;
    cout << endl << "--> Defensa especial: " << this->sDEF;
    cout << endl << "--> Velocidad:        " << this->SPD;
    cout << endl << "--> Exp:              " << this->EXP;
}


string Pokemon::getName()
{
    return this->name;
}


int Pokemon::getHP()
{
    return this->HP;
}


void Pokemon::setHP( int sumar_quitar )
{
    this->HP = this->HP + sumar_quitar;
}


int Pokemon::getDEF()
{
    return this->DEF;
}


string Pokemon::getAtk1_Name ()
{
    return this->atk1_name;
}


string Pokemon::getAtk2_Name ()
{
    return this->atk2_name;
}


string Pokemon::getAtk3_Name ()
{
    return this->atk3_name;
}


string Pokemon::getAtk4_Name ()
{
    return this->atk4_name;
}