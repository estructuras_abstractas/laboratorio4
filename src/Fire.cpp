/**
 * @author Jorge Muñoz Taylor
 * @date 26/01/2020
 * 
 * @file Water.cpp
 * @brief
 */

#include "../include/Fire.h"


string Fire::type()
{
    return "fire";
}

string Fire::strongVs()
{
   return "water";
}

string Fire::weakVs()
{
    return "water";
}