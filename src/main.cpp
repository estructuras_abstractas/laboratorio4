/**
 * @author Jorge Muñoz Taylor
 * @date 26/01/2020
 * 
 * @file main.cpp
 * @brief La función principal del programa.
 */

#include <iostream>
#include "../include/Control.h"

#define PROGRAMA_TERMINO_BIEN 0

using namespace std;

int main()
{
    Control control;
        
    return PROGRAMA_TERMINO_BIEN;
}