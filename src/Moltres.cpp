/**
 * @author Jorge Muñoz Taylor
 * @date 26/01/2020
 * 
 * @file Moltres.h
 * @brief
 */

#include "../include/Moltres.h"
#include "../include/pokeascii.h"

#define MOLTRES 146

Moltres::Moltres()
{
    Pokemon::name    = "Moltres";
    Pokemon::species = "Legendario";
    Pokemon::HP      = 2000;
    Pokemon::ATK     = 150;
    Pokemon::DEF     = 5;
    Pokemon::sATK    = 300;
    Pokemon::sDEF    = 150;
    Pokemon::SPD     = 3;
    Pokemon::EXP     = 0;

    Pokemon::atk1_name = "Garras de fuego";
    Pokemon::atk2_name = "Aliento de lava";
    Pokemon::atk3_name = "Magma";
    Pokemon::atk4_name = "Caricia";
}


void Moltres::print()
{
    //Volador

    if( this->type_flying() == "flying" ){ this->Type = "volador"; }
    else 
    {
        cout << " ERROR del programa";
        return;
    }

    if     ( this->strongVs_flying() == "fire" )     this->strongVs = "fuego "; 
    else if( this->strongVs_flying() == "water" )    this->strongVs = "agua ";
    else if( this->strongVs_flying() == "electric" ) this->strongVs = "electricidad ";
    else if( this->strongVs_flying() == "flying" )   this->strongVs = "volador ";
    else this->strongVs = this->strongVs_flying();

    if     ( this->weakVs_flying() == "fire" )     this->weakVs = "fuego "; 
    else if( this->weakVs_flying() == "water" )    this->weakVs = "agua ";
    else if( this->weakVs_flying() == "electric" ) this->weakVs = "electricidad ";
    else if( this->weakVs_flying() == "flying" )   this->weakVs = "volador ";
    else this->weakVs = this->weakVs_flying();


    //Agua
    if( this->type_fire() == "fire" ){ this->Type = "fuego"; }
    else 
    {
        cout << " ERROR del programa";
        return;
    }

    if     ( this->strongVs_fire() == "fire" )     this->strongVs += "fuego"; 
    else if( this->strongVs_fire() == "water" )    this->strongVs += "agua";
    else if( this->strongVs_fire() == "electric" ) this->strongVs += "electricidad";
    else if( this->strongVs_fire() == "flying" )   this->strongVs += "volador";
    else strongVs = this->strongVs_fire();

    if     ( this->weakVs_fire() == "fire" )     this->weakVs += "fuego"; 
    else if( this->weakVs_fire() == "water" )    this->weakVs += "agua";
    else if( this->weakVs_fire() == "electric" ) this->weakVs += "electricidad";
    else if( this->weakVs_fire() == "flying" )   this->weakVs += "volador";
    else this->weakVs += this->weakVs_fire();


    pokeascii( MOLTRES );

    printInfo();

    cout << endl << endl;

    cout << "--> Tipo:          " << this->Type << endl;
    cout << "--> Fuerte contra: " << this->strongVs << endl;
    cout << "--> Débil contra:  " << this->weakVs;

    cout << endl << endl;

}//Fin de print


string Moltres::type_flying()
{
    return Flying::type();
}


string Moltres::strongVs_flying()
{
    return Flying::strongVs();
}


string Moltres::weakVs_flying()
{
    return Flying::weakVs();
}


string Moltres::type_fire()
{
    return Fire::type();
}


string Moltres::strongVs_fire()
{
    return Fire::strongVs();
}


string Moltres::weakVs_fire()
{
    return Fire::weakVs();
}


void Moltres::atk1 ( Pokemon &other )
{
    cout << "--> " << this->name << " usa garras de fuego! ";
    cout << "sobre " << other.getName();
    cout << endl;
    cout << "--> " << other.getName() << " pierde " << other.getDEF() - 100 << " puntos de HP"; 
    
    other.setHP (other.getDEF() - 100); 
}


void Moltres::atk2 ( Pokemon &other )
{
    cout << "--> " <<  this->name << " usa aliento de lava! ";
    cout << "sobre " << other.getName();
    cout << endl;
    cout << "--> " << other.getName() << " pierde " << other.getDEF() - 125 << " puntos de HP"; 
    
    other.setHP (other.getDEF() - 125); 
}


void Moltres::atk3 ( Pokemon &other )
{
    cout << "--> " <<  this->name << " usa magma! ";
    cout << "sobre " << other.getName();
    cout << endl;
    cout << "--> " << other.getName() << " pierde " << other.getDEF() - 150 << " puntos de HP"; 
    
    other.setHP (other.getDEF() - 150); 
}


void Moltres::atk4 ( Pokemon &other )
{
    cout << "--> " <<  this->name << " usa caricia :3! ";
    cout << "sobre " << other.getName();
    cout << endl;
    cout << "--> " << other.getName() << " pierde " << other.getDEF() - 300 << " puntos de HP"; 
    
    other.setHP (other.getDEF() - 500); 
}