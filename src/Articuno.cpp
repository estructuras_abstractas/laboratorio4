/**
 * @author Jorge Muñoz Taylor
 * @date 26/01/2020
 * 
 * @file Articuno.h
 * @brief
 */

#include "../include/Articuno.h"
#include "../include/pokeascii.h"

#define ARTICUNO 144
Articuno::Articuno()
{
    Pokemon::name    = "Articuno";
    Pokemon::species = "Legendario";
    Pokemon::HP      = 1000;
    Pokemon::ATK     = 100;
    Pokemon::DEF     = 30;
    Pokemon::sATK    = 210;
    Pokemon::sDEF    = 230;
    Pokemon::SPD     = 5;
    Pokemon::EXP     = 0;

    Pokemon::atk1_name = "Tormenta de hielo";
    Pokemon::atk2_name = "Estalactitas";
    Pokemon::atk3_name = "Pisotón de hielo";
    Pokemon::atk4_name = "Besito";
}


void Articuno::print()
{
    //Volador

    if( this->type_flying() == "flying" ){ this->Type = "volador"; }
    else 
    {
        cout << " ERROR del programa";
        return;
    }

    if     ( this->strongVs_flying() == "fire" )     this->strongVs = "fuego "; 
    else if( this->strongVs_flying() == "water" )    this->strongVs = "agua ";
    else if( this->strongVs_flying() == "electric" ) this->strongVs = "electricidad ";
    else if( this->strongVs_flying() == "flying" )   this->strongVs = "volador ";
    else this->strongVs = this->strongVs_flying();

    if     ( this->weakVs_flying() == "fire" )     this->weakVs = "fuego "; 
    else if( this->weakVs_flying() == "water" )    this->weakVs = "agua ";
    else if( this->weakVs_flying() == "electric" ) this->weakVs = "electricidad ";
    else if( this->weakVs_flying() == "flying" )   this->weakVs = "volador ";
    else this->weakVs = this->weakVs_flying();


    //Agua
    if( this->type_water() == "water" ){ this->Type = "agua"; }
    else 
    {
        cout << " ERROR del programa";
        return;
    }

    if     ( this->strongVs_water() == "fire" )     this->strongVs += "fuego"; 
    else if( this->strongVs_water() == "water" )    this->strongVs += "agua";
    else if( this->strongVs_water() == "electric" ) this->strongVs += "electricidad";
    else if( this->strongVs_water() == "flying" )   this->strongVs += "volador";
    else strongVs = this->strongVs_water();

    if     ( this->weakVs_water() == "fire" )     this->weakVs += "fuego"; 
    else if( this->weakVs_water() == "water" )    this->weakVs += "agua";
    else if( this->weakVs_water() == "electric" ) this->weakVs += "electricidad";
    else if( this->weakVs_water() == "flying" )   this->weakVs += "volador";
    else this->weakVs += this->weakVs_water();


    pokeascii( ARTICUNO );

    printInfo();

    cout << endl << endl;

    cout << "--> Tipo:          " << this->Type << endl;
    cout << "--> Fuerte contra: " << this->strongVs << endl;
    cout << "--> Débil contra:  " << this->weakVs;

    cout << endl << endl;

}//Fin de print


string Articuno::type_flying()
{
    return Flying::type();
}


string Articuno::strongVs_flying()
{
    return Flying::strongVs();
}


string Articuno::weakVs_flying()
{
    return Flying::weakVs();
}


string Articuno::type_water()
{
    return Water::type();
}


string Articuno::strongVs_water()
{
    return Water::strongVs();
}


string Articuno::weakVs_water()
{
    return Water::weakVs();
}


void Articuno::atk1 ( Pokemon &other )
{
    cout << "--> " <<  this->name << " usa Tormenta de hielo! ";
    cout << "sobre " << other.getName();
    cout << endl;
    cout << "--> " << other.getName() << " pierde " << other.getDEF() - 125 << " puntos de HP"; 
    
    other.setHP (other.getDEF() - 125); 
}


void Articuno::atk2 ( Pokemon &other )
{
    cout << "--> " <<  this->name << " usa Estalactitas! ";
    cout << "sobre " << other.getName();
    cout << endl;
    cout << "--> " << other.getName() << " pierde " << other.getDEF() - 200 << " puntos de HP"; 
    
    other.setHP (other.getDEF() - 150); 
}


void Articuno::atk3 ( Pokemon &other )
{
    cout << "--> " <<  this->name << " usa pisotón de hielo! ";
    cout << "sobre " << other.getName();
    cout << endl;
    cout << "--> " << other.getName() << " pierde " << other.getDEF() - 250 << " puntos de HP"; 
    
    other.setHP (other.getDEF() - 200); 
}


void Articuno::atk4 ( Pokemon &other )
{
    cout << "--> " <<  this->name << " usa Besito :3! ";
    cout << "sobre " << other.getName();
    cout << endl;
    cout << "--> " << other.getName() << " pierde " << other.getDEF() - 500 << " puntos de HP"; 
    
    other.setHP (other.getDEF() - 500); 
}