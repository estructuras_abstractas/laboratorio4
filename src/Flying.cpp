/**
 * @author Jorge Muñoz Taylor
 * @date 26/01/2020
 * 
 * @file Flying.cpp
 * @brief
 */

#include "../include/Flying.h"


string Flying::type()
{
    return "flying";
}

string Flying::strongVs()
{
   return "insecto planta luchador ";
}

string Flying::weakVs()
{
    return "electric";
}