/**
 * @author Jorge Muñoz Taylor
 * @date 26/01/2020
 * 
 * @file Electric.h
 * @brief Define la clase Electric.
 */

#include "../include/Electric.h"


string Electric::type()
{
    return "electric";
}

string Electric::strongVs()
{
    return "water";
}

string Electric::weakVs()
{
    return "fire";
}