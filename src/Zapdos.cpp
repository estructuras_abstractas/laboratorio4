/**
 * @author Jorge Muñoz Taylor
 * @date 26/01/2020
 * 
 * @file Zapdos.h
 * @brief
 */

#include "../include/Zapdos.h"
#include "../include/pokeascii.h"

#define ZAPDOS 145


Zapdos::Zapdos()
{
    Pokemon::name    = "Zapdos";
    Pokemon::species = "Legendario";
    Pokemon::HP      = 1500;
    Pokemon::ATK     = 115;
    Pokemon::DEF     = 45;
    Pokemon::sATK    = 250;
    Pokemon::sDEF    = 240;
    Pokemon::SPD     = 6;
    Pokemon::EXP     = 0;

    Pokemon::atk1_name = "Aliento de trueno";
    Pokemon::atk2_name = "Relámpago";
    Pokemon::atk3_name = "Ojos eléctricos";
    Pokemon::atk4_name = "Apapacho";
}


void Zapdos::print()
{
    //Volador

    if( this->type_flying() == "flying" ){ this->Type = "volador"; }
    else 
    {
        cout << " ERROR del programa";
        return;
    }

    if     ( this->strongVs_flying() == "fire" )     this->strongVs = "fuego "; 
    else if( this->strongVs_flying() == "water" )    this->strongVs = "agua ";
    else if( this->strongVs_flying() == "electric" ) this->strongVs = "electricidad ";
    else if( this->strongVs_flying() == "flying" )   this->strongVs = "volador ";
    else this->strongVs = this->strongVs_flying();

    if     ( this->weakVs_flying() == "fire" )     this->weakVs = "fuego "; 
    else if( this->weakVs_flying() == "water" )    this->weakVs = "agua ";
    else if( this->weakVs_flying() == "electric" ) this->weakVs = "electricidad ";
    else if( this->weakVs_flying() == "flying" )   this->weakVs = "volador ";
    else this->weakVs = this->weakVs_flying();


    //electrico
    if( this->type_electric() == "electric" ){ this->Type = "eléctrico"; }
    else 
    {
        cout << " ERROR del programa";
        return;
    }

    if     ( this->strongVs_electric() == "fire" )     this->strongVs += "fuego"; 
    else if( this->strongVs_electric() == "water" )    this->strongVs += "agua";
    else if( this->strongVs_electric() == "electric" ) this->strongVs += "electricidad";
    else if( this->strongVs_electric() == "flying" )   this->strongVs += "volador";
    else strongVs = this->strongVs_electric();

    if     ( this->weakVs_electric() == "fire" )     this->weakVs += "fuego"; 
    else if( this->weakVs_electric() == "water" )    this->weakVs += "agua";
    else if( this->weakVs_electric() == "electric" ) this->weakVs += "electricidad";
    else if( this->weakVs_electric() == "flying" )   this->weakVs += "volador";
    else this->weakVs += this->weakVs_electric();


    pokeascii(ZAPDOS);

    printInfo();

    cout << endl << endl;

    cout << "--> Tipo:          " << this->Type << endl;
    cout << "--> Fuerte contra: " << this->strongVs << endl;
    cout << "--> Débil contra:  " << this->weakVs;

    cout << endl << endl;

}//Fin de print


string Zapdos::type_flying()
{
    return Flying::type();
}


string Zapdos::strongVs_flying()
{
    return Flying::strongVs();
}


string Zapdos::weakVs_flying()
{
    return Flying::weakVs();
}


string Zapdos::type_electric()
{
    return Electric::type();
}


string Zapdos::strongVs_electric()
{
    return Electric::strongVs();
}


string Zapdos::weakVs_electric()
{
    return Electric::weakVs();
}


void Zapdos::atk1 ( Pokemon &other )
{
    cout << "--> " <<  this->name << " usa Aliento de trueno! ";
    cout << "sobre " << other.getName();
    cout << endl;
    cout << "--> " << other.getName() << " pierde " << other.getDEF() - 115 << " puntos de HP"; 
    
    other.setHP (other.getDEF() - 115); 
}


void Zapdos::atk2 ( Pokemon &other )
{
    cout << "--> " <<  this->name << " usa Relámpago! ";
    cout << "sobre " << other.getName();
    cout << endl;
    cout << "--> " << other.getName() << " pierde " << other.getDEF() - 144 << " puntos de HP"; 
    
    other.setHP (other.getDEF() - 144); 
}


void Zapdos::atk3 ( Pokemon &other )
{
    cout << "--> " <<  this->name << " usa ojos eléctricos! ";
    cout << "sobre " << other.getName();
    cout << endl;
    cout << "--> " << other.getName() << " pierde " << other.getDEF() - 190 << " puntos de HP"; 
    
    other.setHP (other.getDEF() - 190); 
}


void Zapdos::atk4 ( Pokemon &other )
{
    cout << "--> " <<  this->name << " usa Apapacho :3! ";
    cout << "sobre " << other.getName();
    cout << endl;
    cout << "--> " << other.getName() << " pierde " << other.getDEF() - 600 << " puntos de HP"; 
    
    other.setHP (other.getDEF() - 600); 
}