# Proyecto 4: Herencia y polimorfismo en C++


## Integrante
```
Jorge Muñoz Taylor
```

## Como compilar el proyecto
Una vez descargado el código, ubíquese en la carpeta donde está el código:
```
>>cd {PATH}/laboratorio4
```

Por último ejecute el make:
```
>>make
```

## Ejecutar el programa
Simplemente:
```
./bin/labo4
```
El juego es autoexplicativo, sólo siga las instrucciones que se mostrarán en la terminal.

## PARA Doxygen
Ubíquese en la carpeta donde está el código:
```
>>cd {PATH}/laboratorio4
```

Para compilar el doxygen:
```
>>doxygen Doxyfile
```

Para generar el archivo PDF:
```
>>cd DOCS/latex
>>make
```
Esto generará el archivo PDF con la documentación generada por doxygen. Para verlo:
```
Doble clic al archivo refman.pdf dentro de DOCS/latex
```

## Dependencias

Debe tener instalado CMAKE y MAKE en la máquina:
```
>>sudo apt-get install build-essential
>>sudo apt-get install cmake
```
(El makefile verificará e instalará todo lo siguiente)
La biblioteca gráfica SFML:
```
sudo apt-get install libsfml-dev
```

También doxygen y latex:
```
>>sudo apt install doxygen
>>sudo apt install doxygen-gui
>>sudo apt-get install graphviz
>>sudo apt install texlive-latex-extra
>>sudo apt install texlive-lang-spanish
```